import React, { useState } from "react";
import PropTypes from "prop-types";
import Print from "./print";

function FibonacciSeries(props) {
  const initialState = {
    value: "",
    list: [],
  };
  const [state, setState] = useState(initialState);

  const handleInputChange = (event) => {
    const inputNumebr = Number(event.target.value);
    setState((prevState) => {
      return {
        ...state,
        value: inputNumebr,
      };
    });
  };

  const handleClick = () => {
    const list = fibonacciList(state.value);
    setState((prevState) => {
      return {
        ...prevState,
        list,
      };
    });
  };

  function fibonacciList(number) {
    let fib = [0, 1];
    let data = [1];

    for (let i = 2; i < number; i++) {
      if (fib[i - 1] + fib[i - 2] < number) {
        fib[i] = fib[i - 1] + fib[i - 2];
        data.push(fib[i]);
      } else {
        break;
      }
    }

    return data;
  }

  return (
    <div>
      <div>
        Enter Positive Number:{" "}
        <input
          id="inputNumer"
          value={state.value}
          onChange={(e) => handleInputChange(e)}
        />{" "}
        <button onClick={handleClick}>Submit</button>
        <br />
        <Print list={state.list} />
      </div>
    </div>
  );
}

FibonacciSeries.propTypes = {};

export default FibonacciSeries;
