import React, { useState, useEffect } from "react";
import "./index.css";

function Print(props) {
  const initialState = {
    currentPage: 1,
    listPerPage: 5,
  };
  const [state, setState] = useState(initialState);
  const [currentList, setCurrentList] = useState([]);

  useEffect(() => {
    const lastListIndex = state.currentPage * state.listPerPage;
    const firsListIndex = lastListIndex - state.listPerPage;
    const listCount =
      props.list.length >= 5 ? lastListIndex : props.list.length;
    const currentListItems = props.list.slice(firsListIndex, listCount);
    setCurrentList(currentListItems);
  }, [state, props]);

  const handleClick = (number) => {
    setState((prevState) => {
      return {
        ...prevState,
        currentPage: Number(number),
      };
    });
  };

  const renderList = currentList.map((number, index) => {
    return <li key={index}>{number}</li>;
  });

  const pageNumbers = [];
  for (let i = 1; i <= Math.ceil(props.list.length / state.listPerPage); i++) {
    pageNumbers.push(i);
  }

  const renderPageNumbers = pageNumbers.map((number) => {
    return (
      <li key={number} id={number} onClick={() => handleClick(number)}>
        {number}
      </li>
    );
  });

  return (
    <div>
      <ul id="numbers">{renderList}</ul>
      <ul id="paging-numbers">{renderPageNumbers}</ul>
    </div>
  );
}

export default Print;
