import React from "react";
import "./App.css";
import FibonacciSeries from "./fibonacci-series";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h1>Fibonacci Series</h1>
      </header>
      <FibonacciSeries />
    </div>
  );
}

export default App;
