import React from "react";
import { shallow } from "enzyme";
import App from "../components/App";
import FibonacciSeries from "../components/fibonacci-series";

describe("App component", () => {
  let wrapper;
  beforeEach(() => {
    wrapper = shallow(<App />);
  });

  it("should render title correctly", () => {
    expect(wrapper.find("h1").text()).toEqual("Fibonacci Series");
  });

  it("should render FibonacciSeries component", () => {
    expect(wrapper.find(FibonacciSeries).length).toEqual(1);
  });
});
