import React from "react";
import { shallow } from "enzyme";
import FibonacciSeries from "../../components/fibonacci-series";

describe("BowlingGame component", () => {
  let wrapper;
  beforeEach(() => {
    wrapper = shallow(<FibonacciSeries />);
  });

  it("should render FibonacciSeries component", () => {
    expect(wrapper.find("input").length).toEqual(1);
    expect(wrapper.find("button").length).toEqual(1);
  });
});
